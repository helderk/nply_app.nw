//ndispaly
//---------------------------------------------------------
var DEV = false; //DEV

var fs = require('fs');
var os = require('os');
var win = nw.Window.get();


// MENUS ---------------------------------------------------------
var menubar = new nw.Menu({ type: 'menubar' });

// create MacBuiltin
if(os.platform() === 'darwin'){
    menubar.createMacBuiltin('nply',{
        hideEdit: true,
        hideWindow: true
    });
}else{
  //enter-fullscreen
  win.on('enter-fullscreen', function() {
    win.menu = null;
  });
  //leave-fullscreen
  win.on('restore', function() {
    win.menu = menubar;
  });
}

var file_menu = new nw.Menu();
var menubar_file = new nw.MenuItem({"label":"Opções"});
var modifier = (os.platform() === 'darwin') ? 'cmd' : 'ctrl';

//FULLSCREEN-MENU-------------------------------------------
  var file_Fullscreen_item = new nw.MenuItem({
    "label":"Fullscreen",
    "click":function(){
      win.toggleFullscreen();
    },
    "key":"f",
    "modifiers":modifier
  });
  file_menu.append(file_Fullscreen_item);

//DEV-MENU-------------------------------------------
if(DEV){

  //menu devtools
  var file_showDevTools_item = new nw.MenuItem({
    label:"DevTools",
    click:function(){
    win.showDevTools();
    },
    "key":"d",
    "modifiers":modifier
  });
  file_menu.append(file_showDevTools_item);

  //menu clear cache
  var file_clearCache_item = new nw.MenuItem({
    "label":"Clear Cache",
    "click":function(){
      nw.App.clearCache();
    },
    "key":"x",
    "modifiers":modifier
  });
  file_menu.append(file_clearCache_item);

}
// -------------------------------------------

//menu reload
var file_reload_item = new nw.MenuItem({
  "label":"Reload App",
  "click":function(){
    location.reload();
  },
  "key":"r",
  "modifiers":modifier
});
file_menu.append(file_reload_item);


menubar_file.submenu = file_menu;
menubar.append(menubar_file);
win.menu = menubar;


//FECHAR--------------------------------------------
function fechar(){
    //if (confirm("Fechar App?")) {
      //win.hide();
        //win.close(true);
        nw.App.quit();
  //}
}

// Listen to main window's close event
win.on('close', function() {
  fechar();
});


//---------------------------------------------------------


//---------------------------------------------------------
//função conversao de tamanhos
function format_sizes(bytes,decimals) {
   if(bytes == 0) return '0 Byte';
   var k = 1024;
   var dm = decimals + 1 || 3;
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
   var i = Math.floor(Math.log(bytes) / Math.log(k));
   return (bytes / Math.pow(k, i)).toPrecision(dm) + ' ' + sizes[i];
}

//---------------------------------------------------------
//gerador de strig to hash
String.prototype.hashCode = function() {
    var hash = 0;
    if (this.length == 0) return hash;
    for (i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
};

//---------------------------------------------------------
//Resolve IP local
var interfaces = os.networkInterfaces();
var addresses = [];
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            addresses.push(address.address);
        }
    }
}
//console.log(addresses);


//---------------------------------------------------------
//envia info do dispaly para Sdisplays
function sendinfo(sysInfo){
  if(navigator.onLine){
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'https://nply.eu/displays/rec_display.php', true);
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.onload = function () {
        
        //if(DEV) console.log(this.responseText);
        
        //exequta comandos
        if ((this.status == 200) && (this.readyState == 4) ){
            eval(this.responseText);
        };
    };
    xhr.send('sysInfo='+sysInfo);
  }
}

//---------------------------------------------------------
//prepara infos de sistema
var systeminfo = {};
nw.Screen.Init();
function colectInfo(){
  systeminfo.hostname = os.hostname();
  systeminfo.type = os.type();
  systeminfo.platform = os.platform();
  systeminfo.release = os.release();
  systeminfo.arch = os.arch();
  systeminfo.uptime = os.uptime();
  systeminfo.totalmem = format_sizes(os.totalmem(),3);
  systeminfo.freemem = format_sizes(os.freemem(),3);
  systeminfo.cpus = os.cpus();
  systeminfo.networkIP = addresses;
  systeminfo.useragent = navigator.userAgent;
  //systeminfo.networkInterfaces = os.networkInterfaces();
  systeminfo.screens = nw.Screen.screens;
  var uid = new String(JSON.stringify(systeminfo.hostname+systeminfo.type+systeminfo.platform+systeminfo.release+systeminfo.arch+systeminfo.useragent)).hashCode();
  systeminfo.uid = Math.abs(uid);
  systeminfo.Bmemory = performance.memory;

  var sysInfo = JSON.stringify(systeminfo);
  sysInfo = btoa(sysInfo); 
  return sysInfo;
}

sendinfo(colectInfo());//primeiro
setInterval(function(){ sendinfo(colectInfo()) }, 1000*60);//a cada minuto

//console.log(performance.memory);
//console.log(systeminfo.fingerprint);
//console.log(colectInfo());

//---------------------------------------------------------
//loading webview (REVER na falha de ligação)
function app_onload(webview){  
    var webview = document.getElementById("content");
    var indicator = document.getElementById("load");
    webview.src="https://nply.eu/displays/?uid="+systeminfo.uid;

    var loadstart = function() {
        //indicator.innerText = "loading...";
        //indicator.style.visibility = 'visible'; 
    }
    var loadstop = function() {
        //indicator.innerText = "";
        indicator.style.visibility = 'hidden';
        webview.style.visibility = 'visible';   
    }
    webview.addEventListener("loadstart", loadstart);
    webview.addEventListener("loadstop", loadstop);
  }

//onload APP
onload = function () { 
    var webview = document.getElementById("content");
    var indicator = document.getElementById("load");
    webview.style.visibility = 'hidden';  
    indicator.style.visibility = 'visible';  

    if(navigator.onLine){ 
        app_onload(webview); 
    }else{
        win.setBadgeLabel('offline');
        win.requestAttention(1);
    }
}

//online/offline Listeners
window.addEventListener("online", function(){
    app_onload();
    win.setBadgeLabel('');
});
window.addEventListener("offline", function(){
    win.setBadgeLabel('offline');
    win.requestAttention(1);
});


//DEV---------------------------------------------------------
//actualiza o app ao modificar e gravar (excepto  ndir e conteudo)
if(DEV){rloadapp();}

  function rloadapp(){
    var path = './';
    fs.watch(path, { recursive: true }, function(event, filename) {
      var dir = filename.split('/');
      if (location){
        //nw.App.clearCache();
        location.reload();
      }
    });
  }
